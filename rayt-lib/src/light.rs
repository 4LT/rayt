use crate::{RayVec, Color};

pub enum Light {
    Ambient{ color: Color},
    Point{ color: Color, position: RayVec },
    Ortho{ color: Color, direction: RayVec },
}
