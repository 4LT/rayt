use glam::Vec3;
use std::fmt::{Display, Formatter, Result as FmtResult};

pub type RayScalar = f32;
pub type RayVec = Vec3;
pub type ColorCh = f32;
pub type Color = Vec3;
pub type PixelCh = u8;
pub type Pixel = [PixelCh; 3];

fn max(a: ColorCh, b: ColorCh) -> ColorCh {
    if a > b {
        a
    } else {
        b
    }
}

pub fn color_to_pixel(color: Color) -> Pixel {
    let over_bright = max(max(max(color[0], color[1]), color[2]), 1.0);
    let quantize =
        |color_ch| max(color_ch * 255.0 / over_bright, 0.0) as PixelCh;
    [quantize(color[0]), quantize(color[1]), quantize(color[2])]
}

pub struct Ray {
    pub position: RayVec,
    pub direction: RayVec
}

impl Display for Ray {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "Ray(position: {}, direction: {})",
            self.position, self.direction)
    }
}

pub mod image;

pub mod material;

pub mod intersection;

pub mod shape;

pub mod camera;

pub mod pipeline;

pub mod scene;

pub mod light;
