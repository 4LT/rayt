use crate::{Ray, RayVec, RayScalar};

pub struct PerspectiveCameraRayIter {
    image_width: u32,
    image_height: u32,

    up: RayVec,
    right: RayVec,
    origin: RayVec,

    proj_center: RayVec,

    row: u32,
    col: u32,
    d_row: RayScalar,
    d_col: RayScalar,
}

impl PerspectiveCameraRayIter {
    pub fn new(cam: &PerspectiveCamera, img_w: u32, img_h: u32)
        -> PerspectiveCameraRayIter
    {
        let forward = (cam.look_at - cam.position).normalize();
        let proj_center = cam.position + forward * cam.proj_distance;
        let right = forward.cross(cam.up).normalize();
        let up = right.cross(forward);

        PerspectiveCameraRayIter {
            image_width: img_w,
            image_height: img_h,

            up: up,
            right: right,
            origin: cam.position,

            proj_center: proj_center,

            row: 0,
            col: 0,
            d_row: cam.height() / img_h as RayScalar,
            d_col: cam.width() / img_w as RayScalar,
        }
    }

    fn current_ray(&self) -> Ray {
        let r = self.row as RayScalar
            - (self.image_height as RayScalar - 1.0) / 2.0;
        let c = self.col as RayScalar
            - (self.image_width as RayScalar - 1.0) / 2.0;

        let proj_point = self.proj_center +
            self.right * (c * self.d_col) -
            self.up * (r * self.d_row);

        Ray {
            position: self.origin,
            direction: (proj_point - self.origin).normalize(),
        }
    }
}

impl Iterator for PerspectiveCameraRayIter
{
    type Item = Ray;

    fn next(&mut self) -> Option<Self::Item> {
        if self.row < self.image_height {
            let ray = self.current_ray();
            self.col+= 1;

            if self.col >= self.image_width {
                self.col = 0;
                self.row+= 1;
            }

            Some(ray)
        } else {
            None
        }
    }
}


pub struct PerspectiveCamera {
    pub position: RayVec,
    pub up: RayVec,
    pub look_at: RayVec,

    pub proj_distance: RayScalar,
    proj_width: RayScalar,
    proj_height: RayScalar,
    aspect_ratio: RayScalar,
}

impl PerspectiveCamera {
    pub fn new_fov90(
        pos: RayVec,
        up: RayVec,
        look_at: RayVec,
        aspect_ratio: RayScalar
    ) -> PerspectiveCamera {
        let proj_width = 2.0;
        PerspectiveCamera {
            position: pos,
            up: up,
            look_at: look_at,
            aspect_ratio: aspect_ratio,
            proj_width: proj_width,
            proj_height: proj_width / aspect_ratio,
            proj_distance: 1.0,
        }
    }

    pub fn new_cube_face(
        pos: RayVec,
        up: RayVec,
        look_at: RayVec,
    ) -> PerspectiveCamera {
        Self::new_fov90(pos, up, look_at, 1.0)
    }

    pub fn width(&self) -> RayScalar {
        self.proj_width
    }

    pub fn height(&self) -> RayScalar {
        self.proj_height
    }

    pub fn set_h_fov(&mut self, fov: RayScalar) {
        self.proj_width = 2.0 * self.proj_distance * (fov / 2.0).tan();
        self.proj_height = self.proj_width / self.aspect_ratio;
    }

    pub fn set_v_fov(&mut self, fov: RayScalar) {
        self.proj_height = 2.0 * self.proj_distance * (fov/2.0).tan();
        self.proj_width = self.proj_height * self.aspect_ratio;
    }

    pub fn ray_iter(&self, img_w: u32, img_h: u32) -> PerspectiveCameraRayIter {
        PerspectiveCameraRayIter::new(self, img_w, img_h)
    }
}

