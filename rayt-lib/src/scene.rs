use crate::{Ray, RayScalar, Color};
use crate::shape::Shape;
use crate::light::Light;
use crate::intersection::Intersection;

pub struct Scene<'a> {
    pub shapes: &'a [Box<dyn Shape>],
    pub lights: &'a [Light],
    pub sky: Color,
}

impl<'a> Scene<'a> {
    pub fn new(
        shapes: &'a [Box<dyn Shape>],
        lights: &'a [Light],
        sky: Color
    ) -> Scene<'a> {
        Scene { shapes, lights, sky }
    }

    pub fn color_at(&self, ray: &Ray) -> Color {

        let mut closest: Option<Intersection> = None;

        for shape in self.shapes {
            let current = shape.intersect(ray);
            closest = choose_nearest(&current, &closest).clone();
        }

        match closest {
            Some(intersection) =>
                intersection.shade(self),
            None => self.sky,
        }
    }

    pub fn shadow_hit(&self, ray: &Ray, max_dist: Option<RayScalar>) -> bool {
        if let Some(max_dist) = max_dist {
            for shape in self.shapes {
                if let Some(intersection) = shape.intersect(ray) {
                    if intersection.attributes.distance < max_dist {
                        return true;
                    }
                }
            }
        } else {
            for shape in self.shapes {
                if let Some(_) = shape.intersect(ray) {
                    return true;
                }
            }
        }

        false
    }
}


fn choose_nearest<'i>(
    a: &'i Option<Intersection>,
    b: &'i Option<Intersection>
) -> &'i Option<Intersection> {
    match (a, b) {
        (Some(intrsct_a), Some(intrsct_b)) => {
            if intrsct_a.attributes.distance <
                intrsct_b.attributes.distance
            {
                a
            } else {
                b
            }
        },
        (Some(_), None) => a,
        (None, Some(_)) => b,
        _ => &None,
    }
}
