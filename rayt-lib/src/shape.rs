use crate::{RayScalar, RayVec, Ray};
use crate::intersection::{Intersection, IntersectionAttributes, Transition};
use crate::material::Material;
use std::sync::Arc;

pub type Bounds = (RayVec, RayVec);

pub trait Shape {
    fn intersect(&self, ray: &Ray) -> Option<Intersection>;
    fn bounds(&self) -> Bounds;
    fn center(&self) -> RayVec;
    fn material(&self) -> Arc<dyn Material>;
}

pub struct Sphere {
    center: RayVec,
    radius: RayScalar,
    material: Arc<dyn Material>,
}

impl Sphere {
    pub fn new(center: RayVec, radius: RayScalar, material: Arc<dyn Material>)
            -> Self {
        Sphere { center, radius, material }
    }
}

impl Shape for Sphere {
    fn intersect(&self, ray: &Ray) -> Option<Intersection> {
        let self_to_ray = self.center - ray.position;
        // Quadratic equation, a=1
        let b = 2.0 * ray.direction.dot(self_to_ray);
        let c = self_to_ray.length_squared() - self.radius * self.radius;
        let determinant = b*b - 4.0 * c;

        if determinant <= 0.0 {
            // Ray's line does not intersect sphere
            return None;
        }

        let rt_determinant = determinant.sqrt();
        let dist1 = (b - rt_determinant) / 2.0;
        let dist2 = (b + rt_determinant) / 2.0;

        let (min_dist, transition);

        if dist1 <= 0.0 {
            if dist2 <= 0.0 {
                // Sphere is behind ray
                return None;
            } else {
                min_dist = dist2;
                transition = Transition::Exit;
            }
        } else {
            min_dist = dist1;
            transition = Transition::Enter;
        }

        let position = ray.position + min_dist * ray.direction;

        Some(Intersection {
            attributes: IntersectionAttributes {
                distance: min_dist,
                position: position,
                normal: (position - self.center).normalize(),
                incoming: ray.direction,
                transition: transition,
            },
            material: Arc::clone(&self.material),
        })
    }

    fn center(&self) -> RayVec {
        self.center
    }

    fn bounds(&self) -> Bounds {
        let radius_splat = RayVec::splat(self.radius);
        (self.center - radius_splat, self.center + radius_splat)
    }

    fn material(&self) -> Arc<dyn Material> {
        Arc::clone(&self.material)
    }
}

pub struct Triangle {
    verts: [RayVec; 3],
    material: Arc<dyn Material>,
    center: RayVec,
    normal: RayVec,
    basis: (RayVec, RayVec),
}

impl Triangle {
    pub fn new(verts: [RayVec; 3], material: Arc<dyn Material>) -> Self {
        let a2b = verts[1] - verts[0];
        let a2c = verts[2] - verts[0];

        Triangle {
            verts: verts,
            material: material,
            center: (verts[0] + verts[1] + verts[2]) / 3.0,
            normal: a2b.cross(a2c).normalize(),
            basis: (a2b, a2c),
        }
    }
}

impl Shape for Triangle {
    fn intersect(&self, ray: &Ray) -> Option<Intersection> {
        let (a2b, a2c) = self.basis;
        let n_dot_ray_pos_to_a = self.normal.dot(self.verts[0] - ray.position);
        let n_dot_ray_dir = self.normal.dot(ray.direction);

        if n_dot_ray_dir == 0.0 {
            // Ray is on the plane
            return None;
        }

        let dist = n_dot_ray_pos_to_a / n_dot_ray_dir;

        if dist <= 0.0 {
            // Triangle is behind the ray
            return None;
        }

        let intersect = ray.position + ray.direction * dist;
        let a2i = intersect - self.verts[0];

        let u_dot_v = a2b.dot(a2c);
        let w_dot_u = a2i.dot(a2b);
        let w_dot_v = a2i.dot(a2c);
        let u_dot_u = a2b.length_squared();
        let v_dot_v = a2c.length_squared();
        let denom = u_dot_v*u_dot_v - u_dot_u*v_dot_v;

        let s = (u_dot_v*w_dot_v - v_dot_v*w_dot_u) / denom;

        if s < 0.0 || s > 1.0 {
            // Ray misses on left or right
            return None;
        }

        let t = (u_dot_v*w_dot_u - u_dot_u*w_dot_v) / denom;

        if t < 0.0 || s + t > 1.0 {
            // Ray misses below or beyond diagonal
            return None;
        }

        Some(Intersection {
            attributes: IntersectionAttributes {
                transition: if n_dot_ray_dir < 0.0 {
                    Transition::Enter
                } else {
                    Transition::Exit
                },
                normal: self.normal,
                distance: dist,
                position: intersect,
                incoming: ray.direction,
            },
            material: Arc::clone(&self.material),
        })
    }

    fn bounds(&self) -> Bounds {
        self.verts[1..3].iter()
            .fold(
                (self.verts[0], self.verts[0]),
                |accum, &vert| (accum.0.min(vert), accum.1.max(vert)))
    }

    fn center(&self) -> RayVec {
        self.center
    }

    fn material(&self) -> Arc<dyn Material> {
        Arc::clone(&self.material)
    }
}

