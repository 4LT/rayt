use crate::{Ray, RayScalar, RayVec, Color};
use crate::scene::Scene;
use crate::intersection::{IntersectionAttributes, Transition};
use crate::light::Light;

const EPSILON: RayScalar = 0.001;

pub trait Material {
    fn shade(
        &self,
        attr: &IntersectionAttributes,
        scene: &Scene,
    ) -> Color;
}

pub struct Solid {
    pub color: Color
}

impl Material for Solid {
    fn shade(
        &self,
        _: &IntersectionAttributes,
        _: &Scene,
    ) -> Color {
        self.color
    }
}

pub struct Lambertian {
    pub color: Color
}

impl Material for Lambertian {
    fn shade(
        &self,
        attr: &IntersectionAttributes,
        scene: &Scene,
    ) -> Color {
        self.color * scene.lights.iter()
            .filter(|light| !shadow_hit(attr, light, scene))
            .map(|light| light_lambertian(attr, light))
            .fold(Color::new(0.0, 0.0, 0.0), |accum, clr| accum + clr)
    }
}

fn min(a: RayScalar, b: RayScalar) -> RayScalar {
    if a < b {
        a
    } else {
        b
    }
}

fn max(a: RayScalar, b: RayScalar) -> RayScalar {
    if a > b {
        a
    } else {
        b
    }
}

fn pos_dot(a: RayVec, b: RayVec) -> RayScalar {
    max(a.dot(b), 0.0)
}


fn light_lambertian(
    attr: &IntersectionAttributes,
    light: &Light,
) -> Color {
    match *light {
        Light::Ambient{ color } => color,
        Light::Point{ color, position } => {
            let delta = position - attr.position;
            color /
                (delta).length_squared() *
                pos_dot(delta.normalize(), attr.normal)
        },
        Light::Ortho{ color, direction } => {
            color * pos_dot(-direction, attr.normal)
        },
    }
}

fn shadow_hit (
    attr: &IntersectionAttributes,
    light: &Light,
    scene: &Scene,
) ->  bool {
    match *light {
        Light::Ambient{..} => false,
        Light::Point{ position, .. } => {
            let light2surf = attr.position - position;
            let distance = light2surf.length();

            if distance == 0.0 {
                return false;
            }

            let ray = Ray { position, direction: light2surf/distance };
            scene.shadow_hit(&ray, Some(distance))
        },
        Light::Ortho{ direction, .. } => {
            let ray = Ray {
                position: fudge_attr_position(attr),
                direction: -direction,
            };
            scene.shadow_hit(&ray, None)
        }
    }
}

fn fudge(init_position: RayVec, direction: RayVec) -> RayVec {
    init_position + direction * EPSILON
}

fn fudge_attr_position(attr: &IntersectionAttributes) -> RayVec {
    match attr.transition {
        Transition::Enter => fudge(attr.position, attr.normal),
        Transition::Exit => fudge(attr.position, -attr.normal),
    }
}
