use crate::Pixel;
use core::ops::{Index, IndexMut};

pub struct Image {
    width: u32,
    height: u32,
    pixels: Vec<Pixel>,
}

impl Image {
    pub fn new(w: u32, h: u32) -> Image {
        let length = w as usize * h as usize;

        Image {
            width: w,
            height: h,
            pixels: vec![[0, 0, 0]; length],
        }
    }

    pub fn width(&self) -> u32 { self.width }

    pub fn height(&self) -> u32 { self.height }

    pub fn pixels(&self) -> &Vec<Pixel> { &self.pixels }

    pub fn pixel_at(&self, row: u32, col: u32) -> Pixel {
        let r = row as usize;
        let c = col as usize;
        let w = self.width as usize;
        self.pixels[r*w+c]
    }

    pub fn pixel_at_mut(&mut self, row: u32, col: u32) -> &mut Pixel {
        let r = row as usize;
        let c = col as usize;
        let w = self.width as usize;
        &mut self.pixels[r*w+c]
    }
}

impl Index<usize> for Image {
    type Output = Pixel;

    fn index(&self, index: usize) -> &Self::Output {
        &self.pixels[index]
    }
}

impl IndexMut<usize> for Image {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.pixels[index]
    }
}
