use std::sync::Arc;
use crate::{RayScalar, RayVec, Color};
use crate::scene::Scene;
use crate::material::Material;

#[derive(Copy, Clone)]
pub enum Transition {
    Enter,
    Exit,
}

#[derive(Clone)]
pub struct IntersectionAttributes {
    pub position: RayVec,
    pub normal: RayVec,
    pub incoming: RayVec,
    pub distance: RayScalar,
    pub transition: Transition,
}

#[derive(Clone)]
pub struct Intersection {
    pub attributes: IntersectionAttributes,
    pub material: Arc<dyn Material>,
}

impl Intersection {
    pub fn shade(
        &self,
        scene: &Scene,
    ) -> Color {
        self.material.shade(&self.attributes, scene)
    }
}
