use crate::scene::Scene;
use crate::camera::{PerspectiveCamera, PerspectiveCameraRayIter};
use crate::image::Image;
use crate::color_to_pixel;

pub struct Pipeline<'a> {
    scene: &'a Scene<'a>,
    ray_iter: PerspectiveCameraRayIter,
    image_width: u32,
    image_height: u32,
}

impl<'a> Pipeline<'a> {
    pub fn new(
        scene: &'a Scene,
        camera: &PerspectiveCamera,
        img_w: u32,
        img_h: u32,
    ) -> Pipeline<'a> {
        Pipeline {
            scene: scene,
            ray_iter: camera.ray_iter(img_w, img_h),
            image_width: img_w,
            image_height: img_h,
        }
    }

    pub fn render(self) -> Image {
        let mut img = Image::new(self.image_width, self.image_height);
        let scene = &self.scene;
        let pix_iter = self.ray_iter.map(|ray| scene.color_at(&ray))
            .map(|color| color_to_pixel(color));

        for (index, pixel) in pix_iter.enumerate() {
            img[index] = pixel;
        }

        img
    }
}

