extern crate rayt_lib;
extern crate image;

use std::sync::Arc;

use image::{ImageBuffer, Rgb};

use rayt_lib::{RayVec, RayScalar, Color, PixelCh};
use rayt_lib::pipeline::Pipeline;
use rayt_lib::camera::PerspectiveCamera;
use rayt_lib::scene::Scene;
use rayt_lib::shape::{Sphere, Triangle, Shape};
use rayt_lib::material::{Lambertian, Material};
use rayt_lib::light::Light;

fn main() {
    let width = 640u32;
    let height = 480u32;

    let mut cam = PerspectiveCamera::new_fov90(
        RayVec::new(55.0, -180.0, 40.0),
        RayVec::new(0.0, 0.0, 1.0),
        RayVec::new(63.0, 100.0, 30.0),
        width as RayScalar / height as RayScalar,
    );

    cam.set_h_fov(2.0 * (0.6 as RayScalar).atan());

    let sky_color = Color::new(0.4, 0.7, 1.0);

    let blue_mat = Arc::new(Lambertian{ color: Color::new(0.0, 0.0, 1.0) });
    let green_mat = Arc::new(Lambertian{ color: Color::new(0.0, 0.75, 0.0) });
    let brown_mat: Arc<dyn Material> =
        Arc::new(Lambertian{ color: Color::new(0.75, 0.375, 0.0) });
    
    let sphere1 = Box::new(Sphere::new(
        RayVec::new(55.0, -20.0, 30.0),
        24.0,
        blue_mat,
    )) as Box<dyn Shape>;

    let sphere2 = Box::new(Sphere::new(
        RayVec::new(20.0, 30.0, 22.0),
        18.0,
        green_mat,
    )) as Box<dyn Shape>;

    let floor = (
        Box::new(Triangle::new(
            [
                RayVec::new(-100.0, -220.0, 0.0),
                RayVec::new( 100.0, -220.0, 0.0),
                RayVec::new(-100.0,  220.0, 0.0),
            ],
            Arc::clone(&brown_mat),
        )) as Box<dyn Shape>,
        Box::new(Triangle::new(
            [
                RayVec::new( 100.0, -220.0, 0.0),
                RayVec::new( 100.0,  220.0, 0.0),
                RayVec::new(-100.0,  220.0, 0.0),
            ],
            brown_mat,
        )) as Box<dyn Shape>,
    );

    let sky_light = Light::Ambient{ color: sky_color * 0.4 };
    let sun_light = Light::Ortho{
        color: Color::new(0.6, 0.6, 0.6),
        direction: (
            RayVec::new(55.0, -20.0, 30.0) - RayVec::new(65.0, -80.0, 128.0)
            ).normalize(),
    };

    let shapes = vec![sphere1, sphere2, floor.0, floor.1];
    let lights = vec![sky_light, sun_light];
    let scene = Scene::new(&shapes[..], &lights[..], sky_color);
    let pipeline = Pipeline::new(&scene, &cam, width, height);
    let image = pipeline.render();
    
    let raw: Vec<PixelCh> = image.pixels()
        .into_iter()
        .flatten()
        .cloned()
        .collect();

    let img_buf: ImageBuffer<Rgb<_>, _> =
        ImageBuffer::from_raw(image.width(), image.height(), raw)
        .unwrap();
    img_buf.save("rayt.png").unwrap();
}
